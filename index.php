<?php
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
  if (!empty($_GET['save'])) {
    // Если есть параметр save, то выводим сообщение пользователю.
    print('Спасибо, результаты сохранены.');
  }
  // Включаем содержимое файла form.php.
  include('form.php');
  // Завершаем работу скрипта.
  exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

// Проверяем ошибки.
$errors = FALSE;
if (empty($_POST['fio'])) {
  print('Заполните имя.<br/>');
  $errors = TRUE;
}

// *************
// Тут необходимо проверить правильность заполнения всех остальных полей.
if (empty($_POST['birth'])) {
  print('Заполните год рождения.<br/>');
  $errors = TRUE;
}
if (!isset($_POST['radioS'])) {
  print('Выберите пол.<br/>');
  $errors = TRUE;
}
if (!isset($_POST['radioL'])) {
  print('Выберите к-во конечностей.<br/>');
  $errors = TRUE;
}
if (!isset($_POST['checkbox'])) {
  print('Подтверите согласие.<br/>');
  $errors = TRUE;
}

// *************

if ($errors) {
  // При наличии ошибок завершаем работу скрипта.
  exit();
}

// Сохранение в базу данных.

$user = 'u20838';
$pass = '96345246';
$db = new PDO('mysql:host=localhost;dbname=u20838', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

// Подготовленный запрос. Не именованные метки.
/*try {
  $stmt = $db->prepare("INSERT INTO application (name) SET name = ?");
  $stmt -> execute(array('fio'));
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}*/

//  stmt - это "дескриптор состояния".
 
//  Именованные метки.
$stmt = $db->prepare("INSERT INTO forma (fio, birth,email,sex,limb,god,wall,lev,about,checkbox) VALUES (:fio, :birth,:email,:sex,:limb,:god,:wall,:lev,:about,:checkbox)");
$myabil=$_POST['abil'];
for($i=0;$i<3;$i++)
{
  if($myabil[$i]!=1)
  {
    $myabil[$i]=0;
  }
}
$stmt -> execute(array('fio'=>$_POST['fio'], 'birth'=>$_POST['birth'],'email'=>$_POST['email'],'sex'=>$_POST['radioS'],'limb'=>$_POST['radioL'],'god'=>$myabil[0],'wall'=>$myabil[1],'lev'=>$myabil[2],'about'=>$_POST['about'],'checkbox'=>$_POST['checkbox']));

//Еще вариант
/*$stmt = $db->prepare("INSERT INTO users (firstname, lastname, email) VALUES (:firstname, :lastname, :email)");
$stmt->bindParam(':firstname', $firstname);
$stmt->bindParam(':lastname', $lastname);
$stmt->bindParam(':email', $email);
$firstname = "John";
$lastname = "Smith";
$email = "john@test.com";
$stmt->execute();
*/

// Делаем перенаправление.
// Если запись не сохраняется, но ошибок не видно, то можно закомментировать эту строку чтобы увидеть ошибку.
// Если ошибок при этом не видно, то необходимо настроить параметр display_errors для PHP.
header('Location: ?save=1');
